all: ratdemo

ratdemo: main.o rational.o
	g++ -Wall -ansi -pedantic -g -O2 -o ratdemo main.o rational.o
main.o: main.cpp rational.h 
	g++ -Wall -ansi -pedantic -g -O2 -c main.cpp
rational.o: rational.cpp rational.h
	g++ -Wall -ansi -pedantic -g -O2 -c rational.cpp

clean:
	rm -f *.o myprogram
